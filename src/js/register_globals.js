import * as components from '../components';

export default () => {
    Object.keys(components).forEach(key => {
        Vue.component(key, components[key]);
    });
};
