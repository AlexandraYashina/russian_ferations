export default class CopyrightControl {
    constructor(copyrights) {
        this._copyrights = copyrights;
        this._updateCompact = this._updateCompact.bind(this);
        this._updateStyle = this._updateStyle.bind(this);
    }

    onAdd(map) {
        this._map = map;
        this._container = document.createElement('div');
        this._container.classList.add('mapboxgl-ctrl');
        this._container.classList.add('copyright');
        this._container.classList.add('mapboxgl-ctrl-attrib');
        this._innerContainer = document.createElement('div');
        this._innerContainer.classList.add('mapboxgl-ctrl-attrib-inner');
        this._container.appendChild(this._innerContainer);

        this._map.on('styledata', this._updateStyle);
        this._updateStyle();
        this._map.on('resize', this._updateCompact);
        this._updateCompact();

        return this._container;
    }

    _updateCompact() {
        if (this._map.getCanvasContainer().offsetWidth <= 640) {
            this._container.classList.add('control-compact');
        } else {
            this._container.classList.remove('control-compact');
        }
    }

    _updateStyle() {
        this._innerContainer.innerHTML = '';
        const copyrights = this._copyrights;
        const self = this;
        copyrights.forEach((item, index, copyrights) => {
            const anchor = document.createElement('a');
            anchor.className = 'copyright__link';
            anchor.setAttribute('href', item.href);
            anchor.setAttribute('target', '_blank');
            if (index === copyrights.length - 1) {
                anchor.innerHTML = `${item.text}`;
            } else {
                anchor.innerHTML = `${item.text} | `;
            }
            anchor.onclick = function() {
                this.href = item.href;
                return true;
            };
            self._innerContainer.appendChild(anchor);
        });
    }

    onRemove() {
        document.getElementsByClassName('copyright')[0].remove();
    }
}
