import axios from 'axios';

const api = axios.create({
    baseURL: 'https://ferations.world/api'
});

export default api;
