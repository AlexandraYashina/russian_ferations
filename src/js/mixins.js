import mapboxgl from 'mapbox-gl';
import CopyrightControl from '@js/copyright_control';
import { COMMON_COPYRIGHTS, LOCAL_COPYRIGHTS } from '@constants';

export const common = {
    data() {
        return {
            mapView: {},
            hoveredStateId: null
        };
    },
    computed: {
        mapId() {
            return Number(this.$route.params.mapId);
        },
        localCopyright() {
            return LOCAL_COPYRIGHTS.find((copyright, index) => index === this.mapId - 1);
        },
        copyrights() {
            return this.localCopyright.concat(COMMON_COPYRIGHTS);
        },
        ...mapState('map', ['lang']),
        ...mapGetters('map', ['map', 'mapWorld'])
    },
    methods: {
        async setMapView(id) {
            await this.getMap({ mapId: this.mapId });
            const accessToken = 'pk.eyJ1IjoiZmVyYXRpb25tYXAiLCJhIjoiY2p6aTE0dnYzMTFiMzNucWtlbzlia3RuOCJ9.hT02vkZoF0HjUrbrWczTpA';
            mapboxgl.accessToken = accessToken;
            const { map } = this;
            if (!document.getElementById(id)) return;
            this.mapView = new mapboxgl.Map({
                container: id,
                style: map.style,
                center: [map.center_lng, map.center_lat],
                zoom: map.zoom,
                attributionControl: false,
                minZoom: map.zoom_min,
                maxZoom: map.zoom_max
            });
            this.mapView.addControl(new mapboxgl.NavigationControl());
            this.mapView.addControl(new mapboxgl.ScaleControl({ position: 'top-right' }));

            const copyrightControl = new CopyrightControl(this.copyrights);
            this.mapView.addControl(copyrightControl, 'bottom-right');
            if (this.mapWorld) {
                this.setMapWorldObjects();
            }
        },
        setMapWorldObjects() {
            this.mapView.on('load', () => {
                this.mapView.addSource('world-polygons', {
                    type: 'geojson',
                    generateId: true,
                    data: 'https://api.mapbox.com/datasets/v1/ferationmap/ckvxzrxki0jwm22pjsnmtgk5c/features?access_token=pk.eyJ1IjoiZmVyYXRpb25tYXAiLCJhIjoiY2p6aTE0dnYzMTFiMzNucWtlbzlia3RuOCJ9.hT02vkZoF0HjUrbrWczTpA'
                });

                this.mapView.addLayer({
                    id: 'world-polygons-layer',
                    type: 'fill',
                    source: 'world-polygons',
                    paint: {
                        'fill-color': [
                            'case',
                            ['boolean', ['feature-state', 'hover'], false],
                            'rgba(161, 41, 255, 0.7)',
                            'rgba(161, 41, 255, 0)'
                        ]
                    }
                });

                this.setMapClickHandler('world-polygons-layer');
                this.setMapHoverHandler('world-polygons-layer', 'world-polygons');

                this.mapView.addLayer({
                    id: 'world-polygons-outline',
                    type: 'line',
                    source: 'world-polygons',
                    paint: {
                        'line-color': 'rgba(161, 41, 255, 1)',
                        'line-width': 2
                    }
                });

                this.mapView.addSource('world-lines', {
                    type: 'geojson',
                    generateId: true,
                    data: 'https://api.mapbox.com/datasets/v1/ferationmap/ckwi77qnq09mc21oe0f7n6oeo/features?access_token=pk.eyJ1IjoiZmVyYXRpb25tYXAiLCJhIjoiY2p6aTE0dnYzMTFiMzNucWtlbzlia3RuOCJ9.hT02vkZoF0HjUrbrWczTpA'
                });

                this.mapView.addLayer({
                    id: 'world-lines-layer',
                    type: 'line',
                    source: 'world-lines',
                    paint: {
                        'line-color': 'rgb(161, 41, 255)',
                        'line-width': [
                            'case',
                            ['boolean', ['feature-state', 'hover'], false],
                            3,
                            2
                        ]
                    }
                });

                this.setMapClickHandler('world-lines-layer');
                this.setMapHoverHandler('world-lines-layer', 'world-lines');

                this.mapView.addSource('world-pacific', {
                    type: 'geojson',
                    generateId: true,
                    data: 'https://gist.githubusercontent.com/ferations/2727cd43ae0f8dafef5f588a2e2f4b00/raw'
                });

                this.mapView.addLayer({
                    id: 'world-pacific-layer',
                    type: 'fill',
                    source: 'world-pacific',
                    paint: {
                        'fill-color': [
                            'case',
                            ['boolean', ['feature-state', 'hover'], false],
                            'rgba(161, 41, 255, 0.7)',
                            'rgba(161, 41, 255, 0)'
                        ],
                        'fill-outline-color': 'rgba(161, 41, 255, 1)'
                    }
                });

                this.setMapClickHandler('world-pacific-layer');
                this.setMapHoverHandler('world-pacific-layer', 'world-pacific');

                this.mapView.addLayer({
                    id: 'world-pacific-outline',
                    type: 'line',
                    source: 'world-pacific',
                    paint: {
                        'line-color': 'rgba(161, 41, 255, 1)',
                        'line-width': 2
                    }
                });
            });
        },
        setMapClickHandler(layer) {
            this.mapView.on('click', layer, e => {
                const { title, text, link, author, coordinates, linkText, pictureMain, videoLink } = JSON.parse(e.features[0].properties.project || '{}');
                this.setMapPopupContent({ title, coordinates, link, linkText, author, text, videoLink, image: pictureMain });
                this.showMapPopup();
            });
        },
        setMapHoverHandler(layer, source) {
            this.mapView.on('mousemove', layer, e => {
                this.mapView.getCanvas().style.cursor = 'pointer';
                if (e.features.length > 0) {
                    if (this.hoveredStateId !== null) {
                        this.mapView.setFeatureState(
                            { source, id: this.hoveredStateId },
                            { hover: false }
                        );
                    }
                    this.hoveredStateId = e.features[0].id;
                    this.mapView.setFeatureState(
                        { source, id: this.hoveredStateId },
                        { hover: true }
                    );
                }
                if (document.body.offsetWidth > 860) {
                    const position = e.point.x > 430 ? 'left' : 'right';
                    const { title, coordinates, picturePreview, textPreview } = JSON.parse(e.features[0].properties.project || '{}');
                    this.setMapObjectPreviewContent({ title, coordinates, textPreview, image: picturePreview, position });
                    this.showMapObjectPreview();
                }
            });

            this.mapView.on('mouseleave', layer, () => {
                this.mapView.getCanvas().style.cursor = 'auto';
                if (this.hoveredStateId !== null) {
                    this.mapView.setFeatureState(
                        { source, id: this.hoveredStateId },
                        { hover: false }
                    );
                }
                this.hoveredStateId = null;
                this.hideMapObjectPreview();
                this.clearMapObjectPreviewContent();
            });
        },
        setImage(path) {
            return require(`@images/${path}`);
        },
        setLangText(text) {
            if (!text) return '';
            if (!text.includes('[RU-EN]')) return text;
            if (this.lang === 'ru') {
                const rusText = text.split('[RU-EN]')[0];
                return rusText;
            }
            const engText = text.split('[RU-EN]')[1];
            return engText;
        },
        ...mapActions('map', ['getMap', 'setMapPopupContent', 'showMapPopup', 'showMapObjectPreview', 'setMapObjectPreviewContent', 'hideMapObjectPreview', 'clearMapObjectPreviewContent'])
    }
};
