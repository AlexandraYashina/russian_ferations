export const COMMON_COPYRIGHTS = [
    {
        href: 'https://www.mapbox.com/about/maps/',
        text: '© Mapbox'
    },
    {
        href: 'http://www.openstreetmap.org/copyright/',
        text: '© OSM contributors'
    }
];

export const LOCAL_COPYRIGHTS = [
    [
        {
            href: 'https://energybase.ru/pipeline',
            text: '© Energybase'
        }
    ],
    [
        {
            href: 'https://ib.komisc.ru/add/rivr',
            text: '© РИВР'
        }
    ],
    [
        {
            href: 'https://pro.culture.ru',
            text: 'Минкультуры РФ'
        },
        {
            href: 'https://upmaps.ru',
            text: '© UpMaps'
        }
    ],
    [
        {
            href: 'https://ru.m.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D0%B2%D0%B0%D0%BB%D1%8B_%D0%B2_%D0%91%D0%B5%D1%80%D0%B5%D0%B7%D0%BD%D0%B8%D0%BA%D0%B0%D1%85_%D0%B8_%D0%A1%D0%BE%D0%BB%D0%B8%D0%BA%D0%B0%D0%BC%D1%81%D0%BA%D0%B5',
            text: 'Wikipedia'
        }
    ],
    [],
    [],
    [],
    [],
    []
];
