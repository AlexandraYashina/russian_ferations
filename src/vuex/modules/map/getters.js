export default {
    map: state => state.map,
    mapWorld: state => state.map.id === 9,
    allMaps: state => state.maps,
    parentMaps: (state, getters) => state.maps.filter(childMap => {
        if (getters.map && getters.map.parent_id) {
            return childMap.parent_id === getters.map.parent_id;
        }
        return !childMap.parent_id;
    }),
    customMaps: (state, getters) => {
        return getters.allMaps.filter(item => item.parent_id === getters.map.id);
    },
    galleries: state => state.galleries,
    photoGalleries: state => state.galleries.filter(gallery => gallery.gallery_types_id === 2),
    photos: (state, getters) => {
        const photos = [];
        getters.photoGalleries.forEach(gallery => gallery.assets.forEach(asset => photos.push(asset)));
        return photos;
    },
    videoGalleries: state => state.galleries.filter(gallery => gallery.gallery_types_id === 1),
    videos: (state, getters) => {
        const videos = [];
        getters.videoGalleries.forEach(gallery => gallery.assets.forEach(asset => videos.push(asset)));
        return videos;
    },
    textGalleries: state => state.galleries.filter(gallery => gallery.gallery_types_id === 3),
    texts: (state, getters) => {
        const texts = [];
        getters.textGalleries.forEach(gallery => gallery.assets.forEach(asset => texts.push(asset)));
        return texts;
    }
};
