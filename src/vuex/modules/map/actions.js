import Map from '@api/map';

const getAllMaps = async ({ commit }) => {
    commit('wait/START', 'maps', { root: true });
    const { data } = await Map.getMaps();
    commit('SET_MAPS', { data });
    commit('wait/END', 'maps', { root: true });
};

const getMap = async ({ commit }, { mapId }) => {
    commit('wait/START', 'map', { root: true });
    const { data } = await Map.getMap(mapId);
    commit('SET_MAP', { data });
    commit('wait/END', 'map', { root: true });
};

const getGalleries = async ({ commit }, { mapId }) => {
    commit('wait/START', 'galleries', { root: true });
    const { data } = await Map.getGalleries(mapId);
    commit('SET_GALLERIES', { data });
    commit('wait/END', 'galleries', { root: true });
};

const changeLang = async ({ commit }) => {
    commit('CHANGE_LANG');
};

const showMapObjectPreview = async ({ commit }) => {
    commit('SHOW_MAP_OBJECT_PREVIEW');
};

const hideMapObjectPreview = async ({ commit }) => {
    commit('HIDE_MAP_OBJECT_PREVIEW');
};

const setMapObjectPreviewContent = async ({ commit }, data) => {
    commit('wait/START', 'objectPreview', { root: true });
    commit('SET_MAP_OBJECT_PREVIEW_CONTENT', data);
    commit('wait/END', 'objectPreview', { root: true });
};

const clearMapObjectPreviewContent = async ({ commit }) => {
    commit('wait/START', 'popup', { root: true });
    commit('CLEAR_MAP_OBJECT_PREVIEW_CONTENT');
    commit('wait/END', 'popup', { root: true });
};

const showMapPopup = async ({ commit }) => {
    commit('SHOW_MAP_POPUP');
};

const hideMapPopup = async ({ commit }) => {
    commit('HIDE_MAP_POPUP');
};

const setMapPopupContent = async ({ commit }, data) => {
    commit('wait/START', 'popup', { root: true });
    commit('SET_POPUP_CONTENT', data);
    commit('wait/END', 'popup', { root: true });
};

const clearMapPopupContent = async ({ commit }) => {
    commit('wait/START', 'popup', { root: true });
    commit('CLEAR_POPUP_CONTENT');
    commit('wait/END', 'popup', { root: true });
};

export default {
    getAllMaps,
    getMap,
    getGalleries,
    changeLang,
    setMapPopupContent,
    clearMapPopupContent,
    showMapPopup,
    hideMapPopup,
    showMapObjectPreview,
    hideMapObjectPreview,
    setMapObjectPreviewContent,
    clearMapObjectPreviewContent
};
