export default {
    maps: [],
    map: {},
    galleries: [],
    lang: 'ru',
    mapPopupVisible: false,
    mapObjectPreviewVisible: false,
    mapPopup: {
        title: '',
        coordinates: '',
        link: '',
        author: '',
        image: '',
        text: '',
        videoLink: ''
    },
    mapObjectPreview: {
        title: '',
        coordinates: '',
        textPreview: '',
        image: '',
        position: ''
    }
};
