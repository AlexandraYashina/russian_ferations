const SET_MAP = (state, data) => {
    state.map = data.data;
};

const SET_MAPS = (state, data) => {
    state.maps = data.data;
};

const SET_GALLERIES = (state, data) => {
    state.galleries = data.data;
};

const CHANGE_LANG = state => {
    if (state.lang === 'ru') {
        state.lang = 'en';
    } else {
        state.lang = 'ru';
    }
};

const SET_MAP_OBJECT_PREVIEW_CONTENT = (state, data) => {
    state.mapObjectPreview = data;
};

const CLEAR_MAP_OBJECT_PREVIEW_CONTENT = (state, data) => {
    state.mapObjectPreview = {
        title: '',
        coordinates: '',
        textPreview: '',
        image: ''
    };
};

const SHOW_MAP_OBJECT_PREVIEW = state => {
    state.mapObjectPreviewVisible = true;
};

const HIDE_MAP_OBJECT_PREVIEW = state => {
    state.mapObjectPreviewVisible = false;
};

const SET_POPUP_CONTENT = (state, data) => {
    state.mapPopup = data;
};

const SHOW_MAP_POPUP = state => {
    state.mapPopupVisible = true;
};

const HIDE_MAP_POPUP = state => {
    state.mapPopupVisible = false;
};

const CLEAR_POPUP_CONTENT = state => {
    state.mapPopup = {
        title: '',
        coordinates: '',
        link: '',
        author: '',
        image: '',
        text: ''
    };
};

export default {
    SET_MAP,
    SET_MAPS,
    SET_GALLERIES,
    CHANGE_LANG,
    SET_POPUP_CONTENT,
    CLEAR_POPUP_CONTENT,
    SHOW_MAP_POPUP,
    HIDE_MAP_POPUP,
    SHOW_MAP_OBJECT_PREVIEW,
    HIDE_MAP_OBJECT_PREVIEW,
    SET_MAP_OBJECT_PREVIEW_CONTENT,
    CLEAR_MAP_OBJECT_PREVIEW_CONTENT
};
