import Vue from 'vue';
import App from './App.vue';
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';
import '@styles/index.scss';
import store from '@/vuex';
import router from '@/router';
import VueWait from 'vue-wait';
import { registerGlobals } from '@js';
import Highcharts from 'highcharts';
import HighchartsVue from 'highcharts-vue';
import Heatmap from 'highcharts/modules/heatmap';
import HighchartsMore from 'highcharts/highcharts-more';

Vue.use(PerfectScrollbar);

Vue.config.productionTip = false;

Vue.use(VueWait);

Vue.use(HighchartsVue, { tagName: 'chart' });
Heatmap(Highcharts);
HighchartsMore(Highcharts);

registerGlobals();

new Vue({
    store,
    router,
    wait: new VueWait({ useVuex: true }),
    render: h => h(App)
}).$mount('#app');
