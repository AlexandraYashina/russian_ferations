import api from '@js/api';

class _Map {
    getMaps() {
        return api.get('/map')
            .then(res => res.data)
            .catch(error => {
                return error;
            });
    }

    getMap(id) {
        return api.get(`/map/${id}`)
            .then(res => res.data)
            .catch(error => {
                return error;
            });
    }

    getGalleries(id) {
        return api.get(`/map/${id}/galleries`)
            .then(res => res.data)
            .catch(error => {
                return error;
            });
    }
}

const Map = new _Map();
export default Map;
