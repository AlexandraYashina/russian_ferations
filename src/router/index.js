import Vue from 'vue';
import Router from 'vue-router';

const MainPage = () => import('@components/MainPage.vue');
const AboutPage = () => import('@components/AboutPage.vue');
const HubsPage = () => import('@components/HubsPage.vue');
const MapPage = () => import('@components/MapPage.vue');
const CustomMaps = () => import('@components/CustomMaps.vue');
const MapFull = () => import('@components/MapFull.vue');
const PhotoGallery = () => import('@components/PhotoGallery.vue');
const VideoGallery = () => import('@components/VideoGallery.vue');
const TextGallery = () => import('@components/TextGallery.vue');

Vue.use(Router);

const router = new Router({
    scrollBehavior() { return { x: 0, y: 0 }; },
    routes: [
        {
            path: '/',
            name: 'main',
            component: MainPage
        },
        {
            path: '/about',
            name: 'about',
            component: AboutPage
        },
        {
            path: '/connections',
            name: 'connections',
            component: HubsPage
        },
        {
            path: '/map/:mapId',
            component: {
                render(c) { return c('router-view'); }
            },
            children: [
                {
                    path: '',
                    name: 'map',
                    component: MapPage
                },
                {
                    path: 'full',
                    name: 'full',
                    component: MapFull
                },
                {
                    path: 'custom',
                    name: 'custom',
                    component: CustomMaps
                },
                {
                    path: 'photo',
                    name: 'photo',
                    component: PhotoGallery
                },
                {
                    path: 'video',
                    name: 'video',
                    component: VideoGallery
                },
                {
                    path: 'texts',
                    name: 'texts',
                    component: TextGallery
                }
            ]
        }
    ]
});

window.router = router;

export default router;
