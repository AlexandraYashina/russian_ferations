const path = require('path');
// const StyleLintPlugin = require('stylelint-webpack-plugin');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const resolve = dir => path.join(__dirname, dir);

module.exports = {
    lintOnSave: true,
    outputDir: resolve('dist/'),
    publicPath: '/russian_ferations/', // for gitlab deploy
    configureWebpack: {
        context: resolve(''),
        entry: {
            app: './src/main.js'
        },
        optimization: {
            minimize: false,
            concatenateModules: false
        },
        resolve: {
            alias: {
                '@images': resolve('src/assets'),
                '@api': resolve('src/api'),
                '@js': resolve('src/js'),
                '@constants': resolve('src/constants'),
                '@styles': resolve('src/scss'),
                '@fonts': resolve('src/assets/fonts'),
                '@components': resolve('src/components')
            }
        },
        plugins: [
            new webpack.ProvidePlugin({
                Vue: ['vue', 'default'],
                mapGetters: ['vuex', 'mapGetters'],
                mapActions: ['vuex', 'mapActions'],
                mapMutations: ['vuex', 'mapMutations'],
                mapState: ['vuex', 'mapState']
            }),
            // new StyleLintPlugin({
            //     files: ['**/*.{vue,htm,html,css,sss,less,scss,sass}'],
            //     formatter: stringFormatter,
            //     emitWarning: true
            // }),
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: 'src/index.html',
                inject: true
            })
        ]
    },
    css: {
        loaderOptions: {
            sass: {
                additionalData: '@import "@styles/_variables.scss"; @import "@styles/_mixins.scss";'
            }
        }
    }
};
